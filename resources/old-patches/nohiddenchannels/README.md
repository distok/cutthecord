## DisTok CutTheCord: No Hidden Channels Patch

This patch shows all channels in the channel list, even those you lack permissions to view.

#### Side effects / bugs
- May break channel edits/creates, not sure
- Currently no indicator is given if you can view a channel or not, I plan to prepend their name with smth later
- Currently you can't turn this on or off, I'll also change that eventually

#### Available and tested on:
- 9.0.3
